#!/usr/bin/perl
#
# vim: se ts=4 sw=4 et:
#
# Copyright 2019 Azure Zanculmarktum <zanculmarktum@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# A script to alter debian/control or .dsc file.

use strict;
use warnings;
use File::Basename;
use Dpkg::Control;
use Dpkg::Control::Info;
use Dpkg::BuildProfiles qw(get_build_profiles);
use Dpkg::Deps;

my $progname = basename($0);
my $host_arch = 'amd64';

# Avoids the need to install gcc
$ENV{DEB_HOST_ARCH} = $host_arch;

my $command;
my $available_commands = 'remove'; # separated by |
unless (@ARGV) {
    show_help();
    exit 1;
}
COMMAND: while (@ARGV) {
    $_ = shift;
    if (/^help$/) {
        if ($ARGV[0] && $ARGV[0] =~ /^($available_commands)$/) {
            eval "$1_help()";
            exit 0;
        }
    }
    if (/^($available_commands)$/) {
        $command = $1;
        last COMMAND;
    }
    show_help();
    exit 1;
}

my @build_profiles = get_build_profiles();

my %remove = ();
my $in_place;
my $field;
my $file;
if ($command eq 'remove') {
    while (@ARGV) {
        $_ = shift;
        if (/^-i(.*)$/) {
            $in_place = $1 ? $1 : '';
        }
        elsif (/^--package=(.*)$/) {
            $remove{package} = '(' . join('|', map { s/.*/\Q$&\E/r } split(',', $1)) . ')';
        }
        elsif (/^--archqual=(.*)$/) {
            $remove{archqual} = '(' . join('|', map { s/.*/\Q$&\E/r } split(',', $1)) . ')';
        }
        elsif (/^--relation=(.*)$/) {
            $remove{relation} = '(' . join('|', map { s/.*/\Q$&\E/r } split(',', $1)) . ')';
        }
        elsif (/^--arches=(.*)$/) {
            $remove{arches} = '(' . join('|', map { s/.*/\Q$&\E/r } split(',', $1)) . ')';
        }
        elsif (/^--restrictions=(.*)$/) {
            $remove{restrictions} = '(' . join('|', map { s/.*/\Q$&\E/r } split(',', $1)) . ')';
        }
        else {
            $field = $_;
            $file = shift;
        }
    }
    if (!%remove || !$field || !$file) {
        eval "${command}_help()";
        exit 1;
    }

    my $rfc822;
    my $control;
    if ($file =~ /\.dsc$/) {
        $rfc822 = Dpkg::Control->new(type => CTRL_PKG_SRC);
        $rfc822->load($file);
    }
    else {
        $control = Dpkg::Control::Info->new($file);
        $rfc822 = $control->get_source();
    }
    # Taken from Dpkg::Deps->deps_parse():
    my @dep_list;
    foreach my $dep_and (split(/\s*,\s*/m, $rfc822->{$field})) {
        my @or_list = ();
        foreach my $dep_or (split(/\s*\|\s*/m, $dep_and)) {
            my $dep_simple = Dpkg::Deps::Simple->new($dep_or, build_dep => 1, host_arch => $host_arch);
            my $package = $dep_simple->{package};
            foreach (keys %remove) {
                if ($package =~ /$remove{$_}/) {
                    $dep_simple->{$_} = undef;
                }
            }
            push @or_list, $dep_simple;
        }
        next if not @or_list;
        if (scalar @or_list == 1) {
            push @dep_list, $or_list[0];
        } else {
            my $dep_or = Dpkg::Deps::OR->new();
            $dep_or->add($_) foreach (@or_list);
            push @dep_list, $dep_or;
        }
    }
    my $dep_and = Dpkg::Deps::AND->new();
    foreach my $dep (@dep_list) {
        $dep_and->add($dep);
    }
    $rfc822->{$field} = $dep_and;
    if (defined $in_place) {
        if ($in_place) {
            rename $file, $file.$in_place;
        }
        $rfc822->save($file);
        my $fh;
        if ($control) {
            open ($fh, '>>', $file) unless $fh;
            print $fh "\n$_" foreach (@{$control->{packages}});
        }
        close $fh if $fh;
    }
    else {
        print $rfc822;
        if ($control) {
            print "\n$_" foreach (@{$control->{packages}});
        }
    }
}

sub show_help {
    print STDERR <<EOF;
Usage:
  $progname command
  $progname help command

Commands:
  remove
EOF
}

sub remove_help {
    print STDERR <<EOF;
Usage:
  $progname remove options field1[,field2,field3] file

Options:
  -i[.orig]                        just like -i in sed
  --package=dep1[,dep2,dep3]
  --archqual=dep1[,dep2,dep3]      foo:amd64         ->  foo
  --relation=dep1[,dep2,dep3]      foo (>= 1.2.3)    ->  foo
  --arches=dep1[,dep2,dep3]        foo [!hurd-i386]  ->  foo
  --restrictions=dep1[,dep2,dep3]  foo <cross>       ->  foo
EOF
}
