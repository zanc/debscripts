OPTIONS =
ifneq (${PACKAGE},)
OPTIONS += --package=${PACKAGE}
endif
ifneq (${ARCHQUAL},)
OPTIONS += --archqual=${ARCHQUAL}
endif
ifneq (${RELATION},)
OPTIONS += --relation=${RELATION}
endif
ifneq (${ARCHES},)
OPTIONS += --arches=${ARCHES}
endif
ifneq (${RESTRICTIONS},)
OPTIONS += --restrictions=${RESTRICTIONS}
endif

_sanity_checks = \
	if [ ! -f ${PROGNAME}_${VERSION}.dsc ]; then \
		echo "Missing ${PROGNAME}_${VERSION}.dsc"; \
		exit 1; \
	fi

fix:
	@${_sanity_checks}
ifneq (${OPTIONS},)
	@${DEBSCRIPTS}/alter_control.pl remove ${OPTIONS} Build-Depends ${PROGNAME}_${VERSION}.dsc >dummy.dsc
	@if [ -f work/debian/control ]; then \
		${DEBSCRIPTS}/alter_control.pl remove -i ${OPTIONS} Build-Depends work/debian/control; \
	fi
endif
	$(call fix)

check deps uris fetch: fix
	@${_sanity_checks}
	@opts=""; \
	if [ "$@" = "check" ]; then \
		opts="-s"; \
	elif [ "$@" = "fetch" ]; then \
		opts="-d"; \
	elif [ "$@" = "uris" ]; then \
		opts="-qq --print-uris"; \
	fi; \
	if [ "${BACKPORTS}" = "yes" ]; then \
		opts="-t ${DIST}-backports $$opts"; \
	fi; \
	if [ -f dummy.dsc ]; then \
		apt-get $$opts build-dep ./dummy.dsc; \
	else \
		apt-get $$opts build-dep ./${PROGNAME}_${VERSION}.dsc; \
	fi

unpack:
	@${_sanity_checks}
	@dpkg-source -x ${PROGNAME}_${VERSION}.dsc work

build: unpack fix
	@cd work && dpkg-buildpackage -b -uc -us --changes-option='-DDistribution=${DIST}' ${BUILD_OPTS}

clean:
	@rm -rf work dummy.dsc

distclean:
	@rm -f *.changes *.buildinfo *.deb *.udeb

.PHONY: fix check deps uris fetch unpack build clean distclean
