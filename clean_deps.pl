#!/usr/bin/perl
# vim: se ts=4 sw=4 et:
#
# Copyright 2019 Azure Zanculmarktum <zanculmarktum@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use File::Basename;
use Dpkg::Deps;

my $progname = basename($0);
my $host_arch = 'amd64';

my @deps;
if ($ARGV[0]) {
    while (@ARGV) {
        $_ = shift;
        if (/^(-h|--help)$/) {
            print STDERR <<EOF;
Usage:
  $progname 'foo, bar (>= 1.2.3), baz:any'
  echo 'foo, bar (>= 1.2.3), baz:any' | $progname
EOF
            exit 0;
        }
        push @deps, $_;
    }
}
else {
    while (<STDIN>) {
        chomp;
        push @deps, $_;
    }
}

foreach my $dep (@deps) {
    my @dep_list;
    foreach my $dep_and (split(/\s*,\s*/m, $dep)) {
        my @or_list = ();
        foreach my $dep_or (split(/\s*\|\s*/m, $dep_and)) {
            if ($dep_or =~
                m{\s*                            # skip leading whitespace
                   ([a-zA-Z0-9][a-zA-Z0-9+.-]*)  # package name
                   (?:                           # start of optional part
                     :                           # colon for architecture
                     [a-zA-Z0-9][a-zA-Z0-9-]*    # architecture name
                   )?                            # end of optional part
                   (?:                           # start of optional part
                     \s* \(                      # open parenthesis for version part
                     \s* [^\)\s]+                # do not attempt to parse version
                     \s* \)                      # closing parenthesis
                   )?                            # end of optional part
                   \s*                           # trailing spaces at end
                 }x) {
                push @or_list, $1;
            }
        }
        next if not @or_list;
        if (scalar @or_list == 1) {
            push @dep_list, $or_list[0];
        } else {
            my $first = 0;
            foreach (@or_list) {
                my $pre = "";
                $pre = "| " if $first++;
                push @dep_list, $pre . $_;
            }
        }
    }
    my $first = 0;
    foreach my $d (@dep_list) {
        print " " if $first++;
        print "$d";
    }
    print "\n";
}
